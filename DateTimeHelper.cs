﻿using System;
using System.Linq;

namespace $Namespace$
{
    public static class DateTimeHelper
    {
        public static DateTime AddBusinessHours(this DateTime start, double hours, double hoursPerDay,
            double startingHour)
        {
            // Handle start
            if (start.IsWeekend())
                start = start.This(DayOfWeek.Monday).AddHours(startingHour);
            if (start.TimeOfDay.TotalHours < startingHour)
                start = start.Date.AddHours(startingHour);
            if (start.TimeOfDay.TotalHours > startingHour + hoursPerDay)
                start = start.Tomorrow().AddHours(startingHour);

            // Add hours
            while (hours > 0)
            {
                var hoursLeftToday = startingHour + hoursPerDay - start.TimeOfDay.TotalHours;
                start = hoursLeftToday <= hours ? start.Tomorrow().AddHours(startingHour) : start.AddHours(hours);
                hours -= hoursLeftToday <= hours ? hoursLeftToday : hours;
                if (start.IsWeekend()) start = start.This(DayOfWeek.Monday).AddHours(startingHour);
            }

            return start;
        }

        public static DateTime AddBusinessDays(this DateTime start, double days)
        {
            // Handle start
            start = start.Date;
            if (start.IsWeekend())
            {
                --days;
                start = start.This(DayOfWeek.Monday);
            }

            // Add days
            while (days > 0)
            {
                start = start.AddDays(1);
                if (start.IsWeekend()) start = start.This(DayOfWeek.Monday);
                --days;
            }

            return start;
        }

        public static DateTime Tomorrow(this DateTime today)
        {
            return today.AddDays(1).Date;
        }

        public static DateTime This(this DateTime start, DayOfWeek dayOfWeek)
        {
            return start.DayOfWeek > dayOfWeek
                ? start.AddDays(dayOfWeek + 7 - start.DayOfWeek).Date
                : start.AddDays(dayOfWeek - start.DayOfWeek).Date;
        }

        public static bool IsWeekend(this DateTime date)
        {
            return date.DayOfWeek == DayOfWeek.Saturday || date.DayOfWeek == DayOfWeek.Sunday;
        }

        public static bool IsWeekDay(this DateTime date)
        {
            return date.DayOfWeek != DayOfWeek.Saturday && date.DayOfWeek != DayOfWeek.Sunday;
        }

        public static DateTime Max(params DateTime?[] dates)
        {
            // Remove null
            dates = dates.Except(new DateTime?[] {null}).ToArray();
            if (dates[0] == null) throw new InvalidProgramException("Programming error");

            // Base cases
            switch (dates.Length)
            {
                case 0: throw new ArgumentException("Must have at least one non-null parameter");
                case 1: return dates[0].Value;
            }

            // Recurse to find max
            var allButFirst = dates.Skip(1).ToArray();
            return new DateTime(
                Math.Max(dates[0].Value.Ticks, Max(allButFirst).Ticks)
            );
        }

        public static DateTime Max(params DateTime[] dates)
        {
            // Base cases
            switch (dates.Length)
            {
                case 0: throw new ArgumentException("Must contain at least one date");
                case 1: return dates[0];
            }

            // Recurse to find max
            var allButFirst = dates.Skip(1).ToArray();
            return new DateTime(
                Math.Max(dates[0].Ticks, Max(allButFirst).Ticks)
            );
        }

        public static DateTime Min(params DateTime?[] dates)
        {
            // Remove null
            dates = dates.Except(new DateTime?[] {null}).ToArray();
            if (dates[0] == null) throw new InvalidProgramException("Programming error");

            // Base cases
            switch (dates.Length)
            {
                case 0: throw new ArgumentException("Must have at least one non-null parameter");
                case 1: return dates[0].Value;
            }

            // Recurse to find min
            var allButFirst = dates.Skip(1).ToArray();
            return new DateTime(
                Math.Min(dates[0].Value.Ticks, Min(allButFirst).Ticks)
            );
        }

        public static DateTime Min(params DateTime[] dates)
        {
            // Base cases
            switch (dates.Length)
            {
                case 0: throw new ArgumentException("Must contain at least one date");
                case 1: return dates[0];
            }

            // Recurse to find min
            var allButFirst = dates.Skip(1).ToArray();
            return new DateTime(
                Math.Min(dates[0].Ticks, Min(allButFirst).Ticks)
            );
        }
    }
}
